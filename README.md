This is the test harness for Part 2 of the Kiip Mobile Challenge.  This provides a Cordova Application that uses the Cordova Plugin in Part 2.

This Cordova application was created using the Cordova CLI.  To use this project you'll need to install the Cordova-CLI.  Instructions for this can be found at https://cordova.apache.org/docs/en/latest/guide/cli/index.html#installing-the-cordova-cli.

To run the project, use this command:
cordova emulate android

** You will also need an AVD (Android Virtual Device = Android Emulator) installed on your system.  This requires a partial/full Android SDK installation.

To build the project:
cordova build android

** This will build a debug APK that can be installed onto a device using ADB (Android Debug Bridge), or via other means.

This project requires the HackerNewsSDKCordovaPlugin (HackerNewsSDKCP).  It should already be installed in this repo (in /plugins), but you can re-install the plugin with these commands:
cordova plugin remove HackerNewsSDKCP
cordova plugin add [path-to-kiip-mobile-challenge-cordova]/


