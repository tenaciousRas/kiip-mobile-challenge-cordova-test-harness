This is the source code for Part 2 of the Kiip Mobile Challenge.  This provides a HackerNewsSDK Cordova Plugin with the specified methods:
initSDK()
showHackerNews()

To install the plugin, use the following command to add it to a Cordova-Android project:
cordova plugin add [path-to-kiip-mobile-challenge-cordova]/

To use this installation process your Cordova-Android project must be created by the Cordova-CLI.  Otherwise, you will want to use Plugman to install the plugin.
