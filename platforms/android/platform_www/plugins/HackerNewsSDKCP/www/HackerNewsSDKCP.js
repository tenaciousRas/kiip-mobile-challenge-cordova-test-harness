cordova.define("HackerNewsSDKCP.HackerNewsSDK", function(require, exports, module) { var iSDK = {
    initSDK : function(success, error) {
        cordova.exec(success, error, 'HackerNewsSDKCP', 'initSDK', []);
    },
    showHackerNews : function(success, error) {
        cordova.exec(success, error, 'HackerNewsSDKCP', 'showHackerNews', []);
    }
}
module.exports = iSDK;
window.iSDK = iSDK;
});
