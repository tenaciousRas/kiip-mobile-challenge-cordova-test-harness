package me.kiip.hackernewssdk.cordova;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import me.kiip.hackernewssdk.android.HackerNewsSDK;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class wraps the HackerNews SDK android library.
 */
public class HackerNewsSDKCP extends CordovaPlugin {

    public static final java.lang.String ACTION_INIT_SDK = "initSDK";
    public static final java.lang.String ACTION_SHOW_HACKER_NEWS = "showHackerNews";
    public static final java.lang.String ACTION_DESTROY = "destroy";

    private HackerNewsSDK mHNSDK;
    private CallbackContext callbackContext;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        mHNSDK = HackerNewsSDK.getInstance();
        if (action.equals(ACTION_INIT_SDK)) {
            this.initSDK(callbackContext);
            return true;
        }
        if (action.equals(ACTION_SHOW_HACKER_NEWS)) {
            this.showHackerNews(callbackContext);
            return true;
        }
        if (action.equals(ACTION_DESTROY)) {
            this.destroy();
            return true;
        }
        return false;
    }

    /**
     * Initialize the hackernews SDK.  Loads the Top20 articles.
     * @param callbackContext
     */
    private void initSDK(final CallbackContext callbackContext) {
        mHNSDK.addSDKInitListener(new HackerNewsSDK.SDKInitListener() {
            @Override
            public void onInitError() {
                callbackContext.error("initSDK encountered an error");
            }

            @Override
            public void onInitStart() {
            }

            @Override
            public void onInitProgress(final int progress) {
            }

            @Override
            public void onInitComplete() {
                callbackContext.success("initSDK has completed");
            }
        });
        Context context = this.cordova.getActivity();
        mHNSDK.initSDK(context);
    }

    /**
     * Display Top20 articles list.
     * @param message
     * @param callbackContext
     */
    private void showHackerNews(final CallbackContext callbackContext) {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                mHNSDK.showHackerNews();
                callbackContext.success("showHackerNews has completed");
            }
        });
    }

    /**
     * Call SDK destroy method.
     */
    private void destroy() {
        mHNSDK.onDestroy();
    }
}
